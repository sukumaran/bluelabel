package com.bluelabel.assignment;

/*
 * This source is free to use
 */
import com.bluelabel.assignment.KeyValue;
import com.bluelabel.assignment.Event;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sukumaran
 */
public class TestEvent {

    Event event;

    public TestEvent() {
        event = new Event();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddEvent() {
        KeyValue kv = new KeyValue("key", "val");
        event.addEvent(kv);
        assertEquals(event.getEvents().get(0), kv);
    }

    @Test
    public void testGetEvents() {
        KeyValue kv = new KeyValue("key", "val");
        KeyValue kv1 = new KeyValue("key", "val");
        event.addEvent(kv);
        event.addEvent(kv1);
        assertEquals(event.getEvents().get(0), kv);
        assertEquals(event.getEvents().get(1), kv1);
    }
}
