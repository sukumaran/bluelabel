package com.bluelabel.assignment;

/*
 * This source is free to use
 */
import com.bluelabel.assignment.MessageType;
import com.bluelabel.assignment.KeyValue;
import com.bluelabel.assignment.XMLMessage;
import com.bluelabel.assignment.Event;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sukumaran
 */
public class TestXMLMessage {

    XMLMessage xmlMessage;

    public TestXMLMessage() {
        Event ev = new Event();
        KeyValue kv = new KeyValue("UserPin", "1234");
        ev.addEvent(kv);
        xmlMessage = new XMLMessage(MessageType.Authentication, ev);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testToString() {
        String xmlString = xmlMessage.toString();
        String expectedxmlStr = "<request>"
                + "<EventType>Authentication</EventType>"
                + "<event>"
                + "<UserPin>1234</UserPin>"
                + "</event>"
                + "</request>";
        assertEquals(expectedxmlStr, xmlString);
    }
}
