package com.bluelabel.assignment;

/*
 * This source is free to use
 */
import com.bluelabel.assignment.KeyValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sukumaran
 */
public class KeyValueTest {

    KeyValue keyValue;

    public KeyValueTest() {
        keyValue = new KeyValue("UserPin", "1234");
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetKey() {
        String key = keyValue.getKey();
        assertEquals(key, "UserPin");
    }

    @Test
    public void testGetValue() {
        String val = keyValue.getValue();
        assertEquals(val, "1234");
    }

    @Test
    public void testSetKey() {
        String key = "DeviceVer";
        keyValue.setKey(key);
        String key1 = keyValue.getKey();
        assertEquals(key, key1);
    }

    @Test
    public void testSetValue() {
        String val = "1234";
        keyValue.setValue(val);
        String val1 = keyValue.getValue();
        assertEquals(val, val1);
    }
}
