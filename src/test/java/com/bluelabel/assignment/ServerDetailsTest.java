/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bluelabel.assignment;

import com.bluelabel.assignment.ServerDetails;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sukumaran
 */
public class ServerDetailsTest {

    public ServerDetailsTest() {
        ServerDetails.setConfigPath("./src/main/java/resource/configTest.txt");
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getServerPortTest() {
        int port = 8080;
        assertEquals(ServerDetails.getInstance().getServerPort(), port);
    }

    @Test
    public void getServerIpTest() {
        String ip = "10.100.14.15";
        assertEquals(ServerDetails.getInstance().getServerIP(), ip);
    }
}
