package com.bluelabel.assignment;

/*
 * This source is free to use
 */
import com.bluelabel.assignment.MessageType;
import com.bluelabel.assignment.Message;
import com.bluelabel.assignment.Event;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sukumaran
 */
public class MessageTest {

    Message message;
    Event event;

    public MessageTest() {
        event = new Event();
        message = new Message(MessageType.Authentication, event);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testGetEvent() {
        Event ev = message.getEvent();
        assertEquals(event, ev);
    }

    @Test
    public void testGetMessageType() {
        MessageType mt = message.getMessageType();
        assertEquals(mt, MessageType.Authentication);
    }
}
