/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bluelabel.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author sukumaran
 */
public class LibConfig
{
    private LibConfig() {  }
    
    /**
     * Load a Properties File
     * @param propsFile Config file object
     * @return Properties
     * @throws IOException Throws IOException
     */
    public static Properties load(File propsFile) throws IOException
    {
        Properties props = new Properties();
        try (FileInputStream fis = new FileInputStream(propsFile)) {
            props.load(fis);
        }
        return props;
    }
}
