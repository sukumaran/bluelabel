/*
 * This software is free to use
 */
package com.bluelabel.assignment;

import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 *
 * @author sukumaran
 */
public class XMLMessage extends Message {

    private static final Logger LOG = Logger.getLogger("client.XMLMessage");

    /**
     *
     * @param type message event type
     * @param event message event
     */
    public XMLMessage(MessageType type, Event event) {
        super(type, event);
    }

    /*
     * <request> 
        <EventType>Authentication</EventType>
        <event>
            <UserPin>12345</UserPin>
            <DeviceId>12345</DeviceId>
            <DeviceSer>ABCDE</DeviceSer>
            <DeviceVer>ABCDE</DeviceVer>
            <TransType>Users</TransType>
        </event>
    </request>
     */
    /**
     * @return request in xml formats
     *
     */
    @Override
    public String toString() {
        if (event != null) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
            try {
                db = dbf.newDocumentBuilder();
                Document dom = db.newDocument();
                Element request = dom.createElement("request");
                Element eventTypeNode = dom.createElement("EventType");
                eventTypeNode.appendChild(dom.createTextNode(getMessageType().name()));
                request.appendChild(eventTypeNode);

                Element eventNode = dom.createElement("event");
                ArrayList<KeyValue> events = event.getEvents();
                for (KeyValue keyvalue : events) {
                    Element ele = dom.createElement(keyvalue.getKey());
                    Text text = dom.createTextNode(keyvalue.getValue());
                    ele.appendChild(text);
                    eventNode.appendChild(ele);
                }
                request.appendChild(eventNode);
                dom.appendChild(request);

                try {
                    StringWriter sw = new StringWriter();
                    TransformerFactory tf = TransformerFactory.newInstance();
                    Transformer transformer = tf.newTransformer();
                    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
//                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//                    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                    transformer.transform(new DOMSource(dom), new StreamResult(sw));
                    return sw.toString();
                } catch (IllegalArgumentException | TransformerException ex) {
                    LOG.error(" Error converting to String " + ex);
                }
            } catch (ParserConfigurationException ex) {
                LOG.error(ex);
            }
        }
        return "";
    }
}
