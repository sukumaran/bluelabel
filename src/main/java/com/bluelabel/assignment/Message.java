/*
 * This software is free to use
 */
package com.bluelabel.assignment;

import org.apache.log4j.Logger;

/**
 *
 * @author sukumaran
 */
public class Message implements IMessage {

    private static final Logger LOG = Logger.getLogger("client.Message");
    /**
     * type : Message event type event : Message event Object
     */
    protected final MessageType type;
    protected final Event event;

    /**
     *
     * @param type message event type
     * @param event message event
     */
    public Message(MessageType type, Event event) {
        this.type = type;
        this.event = event;
    }

    /**
     *
     * @return message event
     */
    public Event getEvent() {
        if (LOG.isDebugEnabled()) {
            LOG.debug(" getEvent returns " + this.event);
        }
        return this.event;
    }

    /**
     *
     * @return message event type
     */
    public MessageType getMessageType() {
        if (LOG.isDebugEnabled()) {
            LOG.debug(" getMessageType " + this.type);
        }
        return this.type;
    }
}
