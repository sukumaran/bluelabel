/*
 * This software is free to use
 */
package com.bluelabel.assignment;

import com.bluelabel.utilities.LibConfig;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author sukumaran
 */
public class ServerDetails {

    private String serverIp;
    private int serverPort;
    private static ServerDetails instance;
    private static final Logger LOG = Logger.getLogger("client.ServerDetails");
    private static String CONFIG_PATH = "./src/main/java/resource/config.txt";

    private ServerDetails() {

    }

    /**
     *
     * @return ServerDetails instance
     */
    public static synchronized ServerDetails getInstance() {
        if (instance == null) {
            instance = new ServerDetails();
            instance.init();
        }
        return instance;
    }

    public static void setConfigPath(String path) {
        CONFIG_PATH = path;
    }

    private void init() {
        loadConfig(CONFIG_PATH);
    }

    /**
     *
     * @return server IP
     */
    public String getServerIP() {
        return serverIp;
    }

    /**
     *
     * @return server port
     */
    public int getServerPort() {
        return serverPort;
    }

    private void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    private void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    private void loadConfig(String resourceconfig) {
        try {
            Properties properties = LibConfig.load(new File(resourceconfig));
            if (properties.getProperty("Server.ip") != null) {
                instance.setServerIp(properties.getProperty("Server.ip"));
            }
            if (properties.getProperty("Server.port") != null) {
                instance.setServerPort(Integer.parseInt(properties.getProperty("Server.port")));
            }
            if (LOG.isDebugEnabled()) {
                LOG.debug(" Server IP is " + this.serverIp);
                LOG.debug(" Server Port is " + this.serverPort);
            }

        } catch (IOException | NumberFormatException ex) {
            LOG.error(" Exception " + ex);
        }
    }
}
