/*
 * This software is free to use
 */
package com.bluelabel.assignment;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Properties;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author sukumaran
 */
public class TCPClient {

    private static final Logger LOG = Logger.getLogger("client.TCPClient");
    private static final String LOG4J_PATH = "./src/main/java/resource/log4j.properties";

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {

        BasicConfigurator.configure();
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(LOG4J_PATH));
        } catch (IOException ex) {
            LOG.error("Error in config parsing" + ex);
        }
        PropertyConfigurator.configure(props);

        TCPClient client = new TCPClient();
        BufferedReader in = null;

        try {
            // Creates the server socket
            Socket devSocket = client.createDeviceSocket();

            if (devSocket != null) {
                //Create the IO Channels
                DataOutputStream out = new DataOutputStream(devSocket.getOutputStream());
                in = new BufferedReader(new InputStreamReader(devSocket.getInputStream()));

                IMessage message;
                message = client.getMessage();
                if (LOG.isDebugEnabled()) {
                    LOG.debug(" Dynamically generated message in XML format \n" + message.toString());
                }

                //Sending message  to server
                client.write(out, message.toString());

                //Reading results from server
                String result = client.read(in);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Result from Server:" + result);
                }
                try (PrintWriter writer = new PrintWriter("Result.txt", "UTF-8")) {
                    writer.println(result);
                }
            }
        } catch (IOException ex) {
            LOG.error(" Exception while creating i/o channels " + ex);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                LOG.error(" Exception while closing i/o channels  " + ex);
            }
        }

    }

    private Socket createDeviceSocket() {
        Socket devSock = null;
        String hostAddress = ServerDetails.getInstance().getServerIP();
        int port = ServerDetails.getInstance().getServerPort();
        boolean isLinkUp = isLinkUp();
        if (!isLinkUp) {
            LOG.error("Network is down. Please check your internet connectivity");
            return devSock;
        }
        if (isServerListening(hostAddress, port)) {
            try {
                devSock = new Socket(hostAddress, port);
            } catch (IOException ex) {
                LOG.error(" Exception while creating client device socket " + ex);
            }
        } else {
            LOG.error("Server " + hostAddress + " is not listening to port " + port);
        }
        return devSock;
    }

    /**
     *
     * @param in input stream
     * @return
     * @throws IOException
     */
    private String read(BufferedReader in) throws IOException {
        String result = in.readLine();
        System.out.println("FROM SERVER: " + result);
        return result;
    }

    /**
     *
     * @param out output stream
     * @param message message to send
     * @throws IOException
     */
    private void write(DataOutputStream out, String message) throws IOException {
        out.writeBytes(message + "\n");
    }

    /**
     *
     * @param host server host address
     * @param port Server TCP connection port
     * @return
     */
    private static boolean isServerListening(String host, int port) {
        Socket socket = null;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(host, port), 1000);
            return true;
        } catch (SocketTimeoutException e) {
            return false;
        } catch (IOException e) {
            LOG.error(e);
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    LOG.error(e);
                }
            }
        }
        return false;
    }

    /**
     *
     * @return true if network is up
     * @return false if network is down
     */
    private boolean isLinkUp() {
        Socket socket = null;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress("www.google.com", 80), 1000);
            return true;
        } catch (SocketTimeoutException | ConnectException e) {
            return false;
        } catch (UnknownHostException e) {
        } catch (IOException e) {
            LOG.error(e);
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    LOG.error(e);
                }
            }
        }
        return false;
    }

    private IMessage getMessage() {
        /*Creating event elements*/
        KeyValue userPin = new KeyValue("UserPin", "12345");
        KeyValue deviceId = new KeyValue("DeviceId", "12345");
        KeyValue deviceSer = new KeyValue("DeviceSer", "ABCDE");
        KeyValue deviceVer = new KeyValue("DeviceVer", "ABCDE");
        KeyValue transType = new KeyValue("TransType", "Users");
        Event event = new Event();
        /*Adding event elements*/
        event.addEvent(userPin);
        event.addEvent(deviceId);
        event.addEvent(deviceSer);
        event.addEvent(deviceVer);
        event.addEvent(transType);

        //Coding to interface. Which help us to use same code for different form of message inputs
        IMessage message;
        message = new XMLMessage(MessageType.Authentication, event);
        //message = new JSONMessage(MessageType.Authentication,event);
        return message;
    }
}
