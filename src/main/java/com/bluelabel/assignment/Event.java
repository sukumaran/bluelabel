/*
 * This software is free to use
 */
package com.bluelabel.assignment;

import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author sukumaran
 */
public class Event {

    private static final Logger LOG = Logger.getLogger("client.Event");
    private final ArrayList<KeyValue> events;

    /**
     * Constructor initializing events
     */
    public Event() {
        events = new ArrayList<>();
    }

    /**
     *
     * @param event represents property in event
     */
    public void addEvent(KeyValue event) {
        events.add(event);
    }

    /**
     *
     * @return event
     */
    public ArrayList<KeyValue> getEvents() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("\n Events \n" + this.toString());
        }
        return this.events;
    }

    /**
     *
     * @return return all property from event object
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        //JAVA 8
//        this.events.forEach((kv) -> {
//            sb.append(kv.getKey()).append(":").append(kv.getValue()).append("\n");
//        });
        for(KeyValue kv:this.events){
            sb.append(kv.getKey()).append(":").append(kv.getValue()).append("\n");
        }
        
        return sb.toString();
    }
}
