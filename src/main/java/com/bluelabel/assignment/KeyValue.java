/*
 * This software is free to use
 */
package com.bluelabel.assignment;

import org.apache.log4j.Logger;

/**
 *
 * @author sukumaran
 */
public class KeyValue {

    private static final Logger LOG = Logger.getLogger("client.KeyValue");
    private String key;
    private String value;

    /**
     *
     * @param key key of this property
     * @param value value of this property
     */
    public KeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     *
     * @return key from key value pair
     */
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
