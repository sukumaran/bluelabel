/*
 * This software is free to use
 */
package com.bluelabel.assignment;

import org.apache.log4j.Logger;

/**
 *
 * @author sukumaran
 */
public class JSONMessage extends Message {

    private static final Logger LOG = Logger.getLogger("client.JSONMessage");

    /**
     *
     * @param type message event type
     * @param event message event object
     */
    public JSONMessage(MessageType type, Event event) {
        super(type, event);
    }

    /**
     *
     * @return message request in JSON format
     *
     *
     * {"request":{ "EventType":"Authentication", "event":{ "UserPin":12345,
     * "DeviceId":12345, "DeviceSer":ABCDE, "DeviceVer":ABCDE,
     * "TransType":"Users" } }
     *
     */
    @Override
    public String toString() {
        if (event != null) {
            /*I am not implementing this as this is out of scope for this assignment and waste of time for me*/
        }
        return null;
    }
}
